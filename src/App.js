import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import SearchBar from './components/SearchBar';
import MusicList from './components/MusicList';
import InstitutionsList from './components/InstitutionsList';

class App extends Component {
  render() {
    return (
      <div className="App">
        <SearchBar/>
        <InstitutionsList/>
      </div>
    );
  }
}

export default App;
