

var InstitutionsApi = {


    get allInstitutionsUrl (){
        return "/institutions/all"
    },

    get baseurl(){
        return "http://localhost:3002"
    },

    institutionsList: [],

    listAll: function(){
        return fetch('http://192.168.1.9:3002/institutions/all')
        .then(result => result.json())
        .then(result => {
            this.institutionsList = result;            
            return result
        })
    }
    
};


export default InstitutionsApi;