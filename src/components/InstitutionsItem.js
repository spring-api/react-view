import React, {Component} from 'react';

export default class InstitutionsItem extends Component{
    static defaultProps = {
        institution: {}
    }

    render(){
        var props = this.props,
            institution = props.institution;
        return(
            <tbody>
                <td className="institution">
                    {institution.id}
                </td>
                <td className="institution">
                    {institution.nome}
                </td>
                <td className="institution">
                    {institution.endereco}
                </td>
            </tbody>
        );
    }
}