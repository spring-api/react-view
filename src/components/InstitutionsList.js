



import React, {Component} from 'react';
import InstitutionsApi from '../connector/InstitutionsApi';
import InstitutionItem from './InstitutionsItem';

class InstitutionsList extends Component {
    
    
    constructor(){
        super();
        this.state = {
            institutionsList: [],
        };
    }
    
    componentWillMount(){
        if(!InstitutionsApi.institutionsList.length > 0){
            InstitutionsApi.listAll().then(response => {
                this.setState({institutionsList: response})
            })
        }else{
            this.setState({institutionsList: InstitutionsApi.institutionsList})
        }
    }


    render(){
        var state = this.state,
            institutionsList = state.institutionsList;
        return(
            <div>
                 <table>
                    <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th>Endereco</th>
                    </tr>
                    {
                    institutionsList.map(item => <InstitutionItem institution={item} key={item.id} />)
                    }
                    </table> 
            </div>            
        )
    }

}


export default InstitutionsList;